FROM i386/debian:stable as build
RUN apt-get update && apt-get install -y wget bzip2
RUN wget --progress=dot:giga https://dedcon.simamo.de/bin/dedcon-i686-pc-linux-gnu-1.6.0.tar.bz2 && tar xvjf dedcon-i686-pc-linux-gnu-1.6.0.tar.bz2 -C /

FROM alpine:latest 
WORKDIR /server
COPY --from=build /dedcon-i686-pc-linux-gnu-1.6.0/dedcon /server/dedcon
CMD ["/server/dedcon"]